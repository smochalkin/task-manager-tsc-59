package ru.smochalkin.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.endpoint.*;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.service.SessionService;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractListener {

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected SessionService sessionService;

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull ConsoleEvent event);

    @Nullable
    public Role[] roles(){
        return null;
    }

    protected void printResult(@NotNull final Result result){
        @NotNull String output = "Result: " + result.isSuccess();
        @Nullable final String message = result.getMessage();
        if(!isEmpty(message)) output += "\nMessage: " + message;
        System.out.println(output);
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String description = description();
        if(name != null && !name.isEmpty()) result += name + " ";
        if(description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
