package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractTaskListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String name() {
        return "task-clear";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove all tasks of current user.";
    }

    @Override
    @EventListener(condition = "@taskClearListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        @NotNull final Result result = taskEndpoint.clearTasks(sessionService.getSession());
        printResult(result);
    }

}
