package ru.smochalkin.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractSystemListener;

@Component
public final class AboutListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String arg() {
        return "-a";
    }

    @Override
    @NotNull
    public String name() {
        return "about";
    }

    @Override
    @NotNull
    public String description() {
        return "Display developer info.";
    }

    @Override
    @EventListener(condition = "@aboutListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}
