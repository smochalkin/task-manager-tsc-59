package ru.smochalkin.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.AbstractEntityDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@NoArgsConstructor
public abstract class AbstractDtoRepository<E extends AbstractEntityDto> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    protected AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    public void rollback() {
        entityManager.getTransaction().rollback();
    }

    public void begin() {
        entityManager.getTransaction().begin();
    }

    public void commit() {
        entityManager.getTransaction().commit();
    }

}
