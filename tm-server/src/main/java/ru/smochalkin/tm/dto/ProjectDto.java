package ru.smochalkin.tm.dto;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import ru.smochalkin.tm.listener.JpaEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDto extends AbstractBusinessEntityDto {
}
