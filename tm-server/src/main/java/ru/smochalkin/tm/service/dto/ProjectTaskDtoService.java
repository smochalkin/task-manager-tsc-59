package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.repository.dto.IProjectDtoRepository;
import ru.smochalkin.tm.api.repository.dto.ITaskDtoRepository;
import ru.smochalkin.tm.api.service.IProjectTaskService;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.TaskDto;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyNameException;

import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

@Service
public class ProjectTaskDtoService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public IProjectDtoRepository getProjectRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @NotNull
    public ITaskDtoRepository getTaskRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        taskRepository.bindTaskById(userId, projectId, taskId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        taskRepository.unbindTaskById(userId, projectId, taskId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDto> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        return taskRepository.findTasksByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        taskRepository.removeTasksByProjectId(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        ProjectDto projectDto = projectRepository.findByName(userId, name);
        removeProjectById(projectDto.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
        ProjectDto projectDto = projectRepository.findByIndex(userId, index);
        removeProjectById(projectDto.getId());
    }

}
