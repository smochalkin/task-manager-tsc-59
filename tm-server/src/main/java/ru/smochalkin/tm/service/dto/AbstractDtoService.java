package ru.smochalkin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.dto.AbstractEntityDto;

public abstract class AbstractDtoService<E extends AbstractEntityDto> implements IService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
