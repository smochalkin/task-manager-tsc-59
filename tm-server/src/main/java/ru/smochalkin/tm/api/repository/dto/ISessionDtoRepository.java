package ru.smochalkin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.IRepositoryDto;
import ru.smochalkin.tm.dto.SessionDto;

import java.util.List;

public interface ISessionDtoRepository extends IRepositoryDto<SessionDto> {

    void clear();

    @NotNull
    List<SessionDto> findAll();

    @NotNull List<SessionDto> findAllByUserId(@NotNull String userId);

    @Nullable
    SessionDto findById(@Nullable String id);

    void removeById(@Nullable String id);

    void removeByUserId(@NotNull String userId);

    int getCount();

}
